package be.brammeerten.feigntest.controllers;

import be.brammeerten.feigntest.CouldNotReachException;
import be.brammeerten.feigntest.clients.ClientWithErrorDecoder;
import be.brammeerten.feigntest.clients.ClientWithoutErrorDecoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private ClientWithErrorDecoder clientWithErrorDecoder;

    @Autowired
    private ClientWithoutErrorDecoder clientWithoutErrorDecoder;

    @GetMapping(path = "/handle-error")
    public String causeErrorButHandle() {
        try {
            return clientWithErrorDecoder.getDoesNotExist();
        } catch (CouldNotReachException e) {
            return "Error handled :)";
        }
    }

    @GetMapping(path = "/dont-handle-error")
    public String causeError() {
        try {
            return clientWithoutErrorDecoder.getDoesNotExist();
        } catch (CouldNotReachException e) {
            return "Error handled :(";
        }
    }
}
