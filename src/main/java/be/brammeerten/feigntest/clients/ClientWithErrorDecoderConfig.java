package be.brammeerten.feigntest.clients;

import feign.Feign;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClientWithErrorDecoderConfig {

    @Bean
    public ClientWithErrorDecoder client() {
        return Feign.builder()
                .contract(new SpringMvcContract())
                .errorDecoder(new MyErrorDecoder())
                .target(ClientWithErrorDecoder.class, "http://localhost:8080");
    }

}
