package be.brammeerten.feigntest.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "my-client-error-with-decoder", url = "http://localhost:8080")
public interface ClientWithoutErrorDecoder {


    @RequestMapping(value = "/does-not-exist", method = RequestMethod.GET)
    String getDoesNotExist();
}
