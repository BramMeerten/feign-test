package be.brammeerten.feigntest.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface ClientWithErrorDecoder {

    @RequestMapping(value = "/does-not-exist", method = RequestMethod.GET)
    String getDoesNotExist();
}
