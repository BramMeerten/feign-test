package be.brammeerten.feigntest.clients;

import be.brammeerten.feigntest.CouldNotReachException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class MyErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String s, Response response) {
        return new CouldNotReachException();
    }
}
